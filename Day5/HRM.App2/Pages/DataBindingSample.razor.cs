﻿using HRM.App2.Models;

namespace HRM.App2.Pages
{
    public partial class DataBindingSample
    {
        public Employee Employee { get; set; } = default!;

        string currenttime = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");

        protected override Task OnInitializedAsync()
        {
            Employee = new Employee
            {
                FirstName = "John",
                LastName = "Doe"
            };

            return base.OnInitializedAsync();
        }

        public void HandleClick() { 
            Employee.FirstName = "Ramakant";
            Employee.LastName = "Debata";
        }

        public void UpdateClick()
        {
            currenttime = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
        }

        public void StartClock()
        {
            var timer = new Timer(new TimerCallback(_ =>
            {
                currenttime = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
                //Console.WriteLine(currenttime);
                StateHasChanged();
            }), null, 1000, 1000);
        }
    }
}
