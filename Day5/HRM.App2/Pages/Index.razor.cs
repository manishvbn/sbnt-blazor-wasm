using HRM.App2.Components.Widgets;

namespace HRM.App2.Pages
{
    public partial class Index
    {
        private string Title = "HRM | Home";
        private string Description = "This is the home page of the HRM application";
        public List<Type> Widgets { get; set; } = new List<Type>() { typeof(EmployeeCountWidget), typeof(InboxWidget) };
    }
}