﻿namespace HRM.Api.Domain
{
    public enum Gender
    {
        Male,
        Female,
        Other
    }
}
