﻿using Blazored.LocalStorage;
using HRM.App2.Helper;
using HRM.App2.Models;
using System.Text;
using System.Text.Json;

namespace HRM.App2.Services
{
    public class EmployeeDataService : IEmployeeDataService
    {
        private readonly HttpClient _httpClient;
        private readonly ILocalStorageService _localStorageService;

        public EmployeeDataService(HttpClient httpClient, ILocalStorageService localStorageService)
        {
            _httpClient = httpClient;
            _localStorageService = localStorageService;
        }

        public async Task<Employee> AddEmployee(Employee employee)
        {
            var employeeJson =
               new StringContent(JsonSerializer.Serialize(employee), Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync("api/employee", employeeJson);

            if (response.IsSuccessStatusCode)
            {
                return await JsonSerializer.DeserializeAsync<Employee>(await response.Content.ReadAsStreamAsync());
            }

            return null;
        }

        public async Task DeleteEmployee(int employeeId)
        {
            await _httpClient.DeleteAsync($"api/employee/{employeeId}");
        }

        public async Task<IEnumerable<Employee>> GetAllEmployees()
        {
            //bool refreshRequired = true;

            //if(await _localStorageService.ContainKeyAsync(LocalStorageConstants.DataExpirationKey))
            //{
            //    var postExpiration = await _localStorageService.GetItemAsync<DateTime>(LocalStorageConstants.DataExpirationKey);
            //    if(postExpiration > DateTime.Now)
            //    {
            //        // Cache exists and has not expired
            //        refreshRequired = false; // No need to refresh
            //    }
            //}

            //if(!refreshRequired && await _localStorageService.ContainKeyAsync(LocalStorageConstants.DataKey))
            //{
            //    // Cache is valid, return cached data
            //    return await _localStorageService.GetItemAsync<IEnumerable<Employee>>(LocalStorageConstants.DataKey);
            //}   

            var list = await JsonSerializer.DeserializeAsync<IEnumerable<Employee>>
                (await _httpClient.GetStreamAsync($"api/employee"), new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            
            await _localStorageService.SetItemAsync(LocalStorageConstants.DataKey, list);
            await _localStorageService.SetItemAsync(LocalStorageConstants.DataExpirationKey, DateTime.Now.AddMinutes(5));
            
            return list;
        }

        public async Task<Employee> GetEmployeeDetails(int employeeId)
        {
            return await JsonSerializer.DeserializeAsync<Employee>
                 (await _httpClient.GetStreamAsync($"api/employee/{employeeId}"), new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
        }

        public async Task UpdateEmployee(Employee employee)
        {
            var employeeJson =
                new StringContent(JsonSerializer.Serialize(employee), Encoding.UTF8, "application/json");

            await _httpClient.PutAsync("api/employee", employeeJson);
        }
    }
}
