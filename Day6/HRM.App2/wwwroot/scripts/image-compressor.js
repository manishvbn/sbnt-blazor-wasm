﻿function showAlert(message) {
    alert(message);
}

function compressImage(byteArray, contentType, dotNetReference) {
    const blob = new Blob([new Uint8Array(byteArray)], { type: contentType });

    new Compressor(blob, {
        quality: 0.6,
        success(result) {
            const reader = new FileReader();
            reader.readAsDataURL(result);
            reader.onloadend = function () {
                //const base64data = reader.result;
                //dotNetReference.invokeMethodAsync('OnImageCompressed', base64data);
                dotNetReference.invokeMethodAsync('OnImageCompressed', reader.result);
            };
        }, error(err) {
            console.error(err.message);
        }
    });
}