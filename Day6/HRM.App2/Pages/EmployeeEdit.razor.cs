﻿using HRM.App2.Models;
using HRM.App2.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.JSInterop;

namespace HRM.App2.Pages
{
    public partial class EmployeeEdit
    {
        [Inject]
        public IJSRuntime JSRuntime { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public IEmployeeDataService? EmployeeDataService { get; set; }

        [Inject]
        public ICountryDataService? CountryDataService { get; set; }

        [Inject]
        public IJobCategoryDataService? JobCategoryDataService { get; set; }

        [Parameter]
        public string? EmployeeId { get; set; }

        public Employee Employee { get; set; } = new Employee();
        public List<Country> Countries { get; set; } = new List<Country>();
        public List<JobCategory> JobCategories { get; set; } = new List<JobCategory>();

        protected string Message = string.Empty;
        protected string StatusClass = string.Empty;
        protected bool Saved;

        protected override async Task OnInitializedAsync()
        {
            Saved = false;
            Countries = (await CountryDataService.GetAllCountries()).ToList();
            JobCategories = (await JobCategoryDataService.GetAllJobCategories()).ToList();

            int.TryParse(EmployeeId, out var employeeId);

            if (employeeId == 0)
            {
                Employee = new Employee { CountryId = 1, JobCategoryId = 1, BirthDate = DateTime.Now, JoinedDate = DateTime.Now };
            }
            else
            {
                Employee = await EmployeeDataService.GetEmployeeDetails(int.Parse(EmployeeId));
            }
        }

        protected void NavigateToOverview()
        {
            NavigationManager.NavigateTo("/employeeoverview");
        }

        [JSInvokable]
        public async Task OnImageCompressed(string imageBase64)
        {
            var base64Data = imageBase64.Split(',')[1];
            var bytes = Convert.FromBase64String(base64Data);

            Employee.ImageName = "compressed_" + selectedFile.Name;
            Employee.ImageContent = bytes;

            var addedEmployee = await EmployeeDataService.AddEmployee(Employee);
            if (addedEmployee != null)
            {
                StatusClass = "alert-success";
                Message = "New employee added successfully.";
                Saved = true;
            }
            else
            {
                StatusClass = "alert-danger";
                Message = "Something went wrong adding the new employee. Please try again.";
                Saved = false;
            }

            StateHasChanged();
        }

        protected async Task HandleValidSubmit()
        {
            //await JSRuntime.InvokeVoidAsync("showAlert", "JavaScript Function Called from .NET Code");

            Saved = false;

            if (Employee.EmployeeId == 0) //new
            {
                if (selectedFile != null) //take first image
                {
                    long maxFileSize = 1024 * 1024 * 10; //10 MB
                    var file = selectedFile;
                    Stream stream = file.OpenReadStream(maxFileSize);
                    byte[] buffer = new byte[stream.Length];
                    await stream.ReadAsync(buffer, 0, (int)stream.Length);

                    var dotNetReference = DotNetObjectReference.Create(this);
                    await JSRuntime.InvokeVoidAsync("compressImage", buffer, file.ContentType, dotNetReference);
                }
            }
            else
            {
                await EmployeeDataService.UpdateEmployee(Employee);
                StatusClass = "alert-success";
                Message = "Employee updated successfully.";
                Saved = true;
            }



            //Saved = false;

            //if (Employee.EmployeeId == 0) //new
            //{
            //    if (selectedFile != null) //take first image
            //    {
            //        var file = selectedFile;
            //        Stream stream = file.OpenReadStream();
            //        MemoryStream ms = new();
            //        await stream.CopyToAsync(ms);
            //        stream.Close();

            //        Employee.ImageName = file.Name;
            //        Employee.ImageContent = ms.ToArray();
            //    }

            //    var addedEmployee = await EmployeeDataService.AddEmployee(Employee);
            //    if (addedEmployee != null)
            //    {
            //        StatusClass = "alert-success";
            //        Message = "New employee added successfully.";
            //        Saved = true;
            //    }
            //    else
            //    {
            //        StatusClass = "alert-danger";
            //        Message = "Something went wrong adding the new employee. Please try again.";
            //        Saved = false;
            //    }
            //}
            //else
            //{
            //    await EmployeeDataService.UpdateEmployee(Employee);
            //    StatusClass = "alert-success";
            //    Message = "Employee updated successfully.";
            //    Saved = true;
            //}

        }

        protected async Task HandleInvalidSubmit()
        {
            StatusClass = "alert-danger";
            Message = "There are some validation errors. Please try again.";
        }

        protected async Task DeleteEmployee()
        {
            await EmployeeDataService.DeleteEmployee(Employee.EmployeeId);
            StatusClass = "alert-success";
            Message = "Deleted successfully";

            Saved = true;
        }

        private IBrowserFile selectedFile;

        protected void OnInputFileChange(InputFileChangeEventArgs e)
        {
            selectedFile = e.File;
            StateHasChanged();
        }
    }
}
