﻿using Microsoft.AspNetCore.Components;

namespace BlazorAppTL.Pages
{
    public partial class Index
    {
        MarkupString helloString;

        void SayHello() { 
            string message = string.Format("<br/>Hello from <strong>Telerik Blazor</strong> at {0}.<br /> Now you can use C# to write front-end!", DateTime.Now);
            helloString = new MarkupString(message);
        }
    }
}
