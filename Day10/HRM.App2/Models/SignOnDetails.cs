﻿namespace HRM.App2.Models
{
    public class SignOnDetails
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
