﻿namespace HRM.App2.Models
{
    public class JobCategory
    {
        public int JobCategoryId { get; set; }
        public string JobCategoryName { get; set; } = string.Empty;
    }
}
