using HRM.App2.Models;
using Microsoft.AspNetCore.Components;

namespace HRM.App2.Components
{
    public partial class EmployeeCard
    {
        [Parameter]
        public Employee Employee { get; set; } = default!;

        [Parameter]
        public EventCallback<Employee> EmployeeQuickViewClicked { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        public void NavigateToDetails(Employee selectedEmployee)
        {
            NavigationManager.NavigateTo($"/employeedetail/{selectedEmployee.EmployeeId}");
        }

        protected override void OnInitialized()
        {
            if (string.IsNullOrEmpty(Employee.LastName))
                throw new ArgumentException("Employee last name is required", nameof(Employee.LastName));
        }
    }
}