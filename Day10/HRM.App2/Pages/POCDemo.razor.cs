﻿using HRM.App2.Models;

namespace HRM.App2.Pages
{
    public partial class POCDemo
    {
        private List<OrganizationItem> orgItems = new List<OrganizationItem>
        {
            new OrganizationItem { Id = 1, Name = "Company", ParentId = null, HasChildren = true },
            new OrganizationItem { Id = 2, Name = "HR Department", ParentId = 1, HasChildren = false },
            new OrganizationItem { Id = 3, Name = "IT Department", ParentId = 1, HasChildren = true },
            new OrganizationItem { Id = 4, Name = "Development Team", ParentId = 3, HasChildren = false },
            new OrganizationItem { Id = 5, Name = "Support Team", ParentId = 3, HasChildren = false }
            // Add more items as needed
        };

        private int SelectedOrgItemId { get; set; }

        protected override async Task OnInitializedAsync()
        {
            orgItems = await Task.FromResult(orgItems);
            SelectedOrgItemId = 4;
        }

        private List<Product> productList = new List<Product>
        {
            new Product { Id = 1, Name = "Laptop" },
            new Product { Id = 2, Name = "Smartphone" },
            new Product { Id = 3, Name = "Tablet" }
        };
        private int selectedProductId;

        private void OnProductSelected(int newValue)
        {
            selectedProductId = newValue;
        }

        private SignOnDetails signOnDetails = new SignOnDetails();

        private void HandleDetailsChange()
        {
            Console.WriteLine("Username: {0}", signOnDetails.Username);
            Console.WriteLine("Password: {0}", signOnDetails.Password);
        }

        private void OnBlur()
        {
            Console.WriteLine("Lost Focus Fired...");
        }

        private List<int> selectedProductIds = new List<int>();

        private void OnProductsSelected(List<int> newValues)
        {
            selectedProductIds = newValues;
            Console.WriteLine(newValues.Count);
        }

        private List<int> selectedProducts = new List<int>();
    }

    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class OrganizationItem
    {
        public int Id { get; set; }
        public int? ParentId { get; set; } // Nullable for root items
        public string Name { get; set; }
        public bool HasChildren { get; set; } // Indicates if this item has child items
    }
}
