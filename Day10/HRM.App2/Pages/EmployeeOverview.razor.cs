using HRM.App2.Models;
using HRM.App2.Services;
using Microsoft.AspNetCore.Components;
using Telerik.Blazor.Components;

namespace HRM.App2.Pages
{
    public partial class EmployeeOverview
    {
        private Employee? _selectedEmployee;
        private string Title = "HRM | Employee Overview";
        private string Description = "This is the Employee Overview page";
        public List<Employee> Employees { get; set; } = default!;
        public List<Country> Countries { get; set; } = new List<Country>();
        public List<JobCategory> JobCategories { get; set; } = new List<JobCategory>();

        [Inject]
        public IEmployeeDataService? EmployeeDataService { get; set; }

        [Inject]
        public ICountryDataService? CountryDataService { get; set; }

        [Inject]
        public IJobCategoryDataService? JobCategoryDataService { get; set; }

        //protected override void OnInitialized()
        //{
        //    Employees = MockDataService.Employees;
        //}

        protected override async Task OnInitializedAsync()
        {
            Countries = (await CountryDataService.GetAllCountries()).ToList();
            JobCategories = (await JobCategoryDataService.GetAllJobCategories()).ToList();
            await GetGridData();
        }
        protected async Task GetGridData()
        {
            Employees = (await EmployeeDataService.GetAllEmployees()).ToList();
        }

        //public void ShowQuickViewPopup(Employee selectedEmployee)
        //{
        //    _selectedEmployee = selectedEmployee;
        //}

        void EditHandler(GridCommandEventArgs args)
        {
            Employee item = (Employee)args.Item;
            Console.WriteLine("Edit event is fired.");
        }

        async Task UpdateHandler(GridCommandEventArgs args)
        {
            Employee item = (Employee)args.Item;
            await EmployeeDataService.UpdateEmployee(item);
            await GetGridData();
            Console.WriteLine("Update event is fired.");
        }
        async Task DeleteHandler(GridCommandEventArgs args)
        {
            Employee item = (Employee)args.Item;
            await EmployeeDataService.DeleteEmployee(item.EmployeeId);
            await GetGridData();
            Console.WriteLine("Delete event is fired.");
        }

        async Task CreateHandler(GridCommandEventArgs args)
        {
            Employee item = (Employee)args.Item;
            await EmployeeDataService.AddEmployee(item);
            await GetGridData();
            Console.WriteLine("Create event is fired.");
        }
        async Task CancelHandler(GridCommandEventArgs args)
        {
            Console.WriteLine("Cancel event is fired.");
        }
    }
}