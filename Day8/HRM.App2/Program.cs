using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using HRM.App2;
using HRM.App2.Services;
using Blazored.LocalStorage;
using HRM.App2.State;
using HRM.App2.Helper;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddLocalization();
builder.Services.AddBlazoredLocalStorage();
builder.Services.AddScoped<ApplicationState>();
builder.Services.AddTransient<CustomAuthorizationMessageHandler>();

//builder.Services.AddHttpClient<IEmployeeDataService, EmployeeDataService>(client => client.BaseAddress = 
//new Uri("https://localhost:7112/"));
//builder.Services.AddHttpClient<IJobCategoryDataService, JobCategoryDataService>(client => client.BaseAddress = 
//new Uri("https://localhost:7112/"));
//builder.Services.AddHttpClient<ICountryDataService, CountryDataService>(client => client.BaseAddress = 
//new Uri("https://localhost:7112/"));

builder.Services.AddHttpClient<IEmployeeDataService, EmployeeDataService>(client => client.BaseAddress =
new Uri(builder.Configuration["ApiSettings:BaseUrl"])).AddHttpMessageHandler<CustomAuthorizationMessageHandler>();
builder.Services.AddHttpClient<IJobCategoryDataService, JobCategoryDataService>(client => client.BaseAddress =
new Uri(builder.Configuration["ApiSettings:BaseUrl"]));
builder.Services.AddHttpClient<ICountryDataService, CountryDataService>(client => client.BaseAddress =
new Uri(builder.Configuration["ApiSettings:BaseUrl"]));

builder.Services.AddOidcAuthentication(options =>
{
    builder.Configuration.Bind("Auth0", options.ProviderOptions);
    options.ProviderOptions.ResponseType = "code";
    options.ProviderOptions.AdditionalProviderParameters.Add("audience", builder.Configuration["Auth0:Audience"]);
});

await builder.Build().RunAsync();
