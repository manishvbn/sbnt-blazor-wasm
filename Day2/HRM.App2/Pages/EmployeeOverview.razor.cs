using HRM.App2.Models;
using HRM.App2.Services;

namespace HRM.App2.Pages
{
    public partial class EmployeeOverview
    {
        private string Title = "HRM | Employee Overview";
        private string Description = "This is the Employee Overview page";
        public List<Employee> Employees { get; set; } = default!;
        protected override void OnInitialized()
        {
            Employees = MockDataService.Employees;
        }
    }
}