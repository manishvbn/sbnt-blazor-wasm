﻿using HRM.Api.Domain;

namespace HRM.Api.Repository
{
    public interface ICountryRepository
    {
        IEnumerable<Country> GetAllCountries();
        Country GetCountryById(int countryId);
    }
}
