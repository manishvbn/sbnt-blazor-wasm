﻿namespace HRM.App2.Helper
{
    public class LocalStorageConstants
    {
        public const string DataExpirationKey = "DataExpirationKey";
        public const string DataKey = "DataKey";
    }
}
