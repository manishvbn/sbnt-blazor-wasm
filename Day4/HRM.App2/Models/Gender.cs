﻿namespace HRM.App2.Models
{
    public enum Gender
    {
        Male,
        Female,
        Other
    }
}
