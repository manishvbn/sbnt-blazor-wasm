﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Telerik.SvgIcons;

namespace HRM.App2.Shared
{
    public partial class MainLayout
    {
        [Inject]
        public AuthenticationStateProvider? AuthenticationStateProvider { get; set; }
        List<DrawerItem> NavigablePages { get; set; } = new List<DrawerItem>
        {
            new DrawerItem { Text = "Home", Url = "/", Icon = SvgIcon.Home},
            new DrawerItem { Text = "Data Binding", Url = "/databindingsample", Icon = SvgIcon.Bold},
        };

        protected override void OnInitialized()
        {
            AuthenticationStateProvider.AuthenticationStateChanged += AuthenticationStateProvider_AuthenticationStateChanged;
        }

        private async void AuthenticationStateProvider_AuthenticationStateChanged(Task<AuthenticationState> task)
        {
            var authenticationState = await task;
            var user = authenticationState.User;

            if(user.Identity.IsAuthenticated)
            {
                NavigablePages.Add(new DrawerItem { Text = "Employees", Url = "employeeoverview", Icon = SvgIcon.Accessibility });
                NavigablePages.Add(new DrawerItem { Text = "Add New Employee", Url = "employeeedit", Icon = SvgIcon.Plus });
            }

            StateHasChanged();
        }

        public class DrawerItem
        {
            public string Text { get; set; }
            public string Url { get; set; }
            public ISvgIcon Icon { get; set; }
            public bool Separator { get; set; }
        }
    }
}
