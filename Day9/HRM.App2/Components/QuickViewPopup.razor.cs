using HRM.App2.Models;
using Microsoft.AspNetCore.Components;

namespace HRM.App2.Components
{
    public partial class QuickViewPopup
    {
        private Employee? _employee;

        [Parameter]
        public Employee Employee { get; set; } = default!;

        protected override void OnParametersSet()
        {
            _employee = Employee;
        }

        public void Close() 
        {
            _employee = null;
        }
    }
}