using Microsoft.AspNetCore.Components;

namespace HRM.App2.Components
{
    public partial class ProfilePicture
    {
        [Parameter]
        public RenderFragment? ChildContent { get; set; }
    }
}