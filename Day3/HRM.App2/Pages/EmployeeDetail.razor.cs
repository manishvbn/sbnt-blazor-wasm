using HRM.App2.Models;
using HRM.App2.Services;
using Microsoft.AspNetCore.Components;

namespace HRM.App2.Pages
{
    public partial class EmployeeDetail
    {
        private string Title = "HRM | Employee Detail";
        private string Description = "This is the Employee Detail page";
        public Employee? Employee { get; set; }

        [Parameter]
        public string EmployeeId { get; set; }

        protected override Task OnInitializedAsync()
        {
            Employee = MockDataService.Employees.FirstOrDefault(e=>e.EmployeeId==int.Parse(EmployeeId));
            return base.OnInitializedAsync();
        }
    }
}