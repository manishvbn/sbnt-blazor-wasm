﻿namespace HRM.App2.Models
{
    public enum MaritalStatus
    {
        Married,
        Single,
        Other
    }
}